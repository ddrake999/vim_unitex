__all__ = ["subsuperscripts_table", "combiningmarks_table",
           "replacements_table", "replace"]

from .unitex import subsuperscripts_table, combiningmarks_table
from .unitex import replacements_table, replace
