from vim_unitex import replace

def test_subscripts():
    assert replace('x_{ij}') == 'xᵢⱼ'
    assert replace(r'\beta_1') == 'β₁'

def test_superscripts():
    assert replace('e^{x+y}') == 'eˣ⁺ʸ'
    assert replace('x^y') == 'xʸ'

def test_combiningmarks():
    assert replace(r'\vec{u}') == 'u⃗'
    assert replace(r'\hat{\dot{v}}') == 'v̂̇̂'

def test_happiness():
    assert replace(r'\smile') == '☺'

def test_fraction():
    assert replace(r'\frac{1}{8}') == '⅛'

def test_inclusion():
    assert replace(r'\mathbb{Q} \subsetneq \mathbb{R}') == 'ℚ ⊊ ℝ'

def test_multiples():
    assert replace(r'\vec{u} + (\vec{v} - \vec{w}) = A \vec{x}') \
            == 'u⃗ + (v⃗ - w⃗) = A x⃗'

def test_mix():
    assert replace(r'e^{x+x} - \frac{1}{2} = \sqrt{x}') == 'eˣ⁺ˣ - ½ = √{x}'
