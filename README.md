# README #

### Purpose ###

Latex to Unicode conversion intended for use in Vim.

This utility is written for Python3 and based on the regular expressions
approach of [Unicodeit](https://github.com/svenkreiss/unicodeit), so it can
handle subscripts and superscripts in braces, like `x^{y+z}` xʸ⁺ᶻ and `A_{ij}`
Aᵢⱼ.  It can also handle nested replacements such as `\vec{\dot{u}}` u⃗̇⃗.
If you're having a rough day, you can even get a `\smile` ☺

### Requirements ###

- Python 3.6 or above
- Linux, OSX, Windows, others?

### Set up ###

Depending on your Python3 installation, one of these should work assuming that
the directory where Pip installs executable files (e.g. in Linux:
`$HOME/.local/bin`) is in your `$PATH`

```
pip3 install --user vim_unitex
pip install vim_unitex
```

You can check that it's installed by typing into a terminal
```
tex2unicode '\beta^2'
```

If you add this (or something similar to suit your taste) to your .vimrc:

```vimscript
function Unitex()
    let s = input("Latex: ")
    return system('tex2unicode "' . s . '"')
endfunction

inoremap <leader>l <C-R>=Unitex()<C-M>
```

then in insert mode, you can type `\l` (assuming your `<leader>` is the
default `\`), to bring up an input prompt where you can type in Latex 

If you'd like to see tables of available replacements, you can run these
commands in a terminal.  The general replacements table is a bit long so you
might want to send it to a file like this: `tex2unicode 'rt' > tmp.txt`.

```
tex2unicode 'rt'    # general replacements table
tex2unicode 'st'    # superscripts and subscripts table
tex2unicode 'ct'    # combining marks table
```

### Contributing ###

Feel free to Fork this repository.  If you want to send a pull request,
I'll be happy to take a look when I have a chance.
If you think you found a bug or have a feature request, please use the
 [Issues Tracker](reporhttps://bitbucket.org/ddrake999/vim_unitex/issues)
for this repository.
